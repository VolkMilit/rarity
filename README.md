# Depricated, you can use Booze directly now

# Rarity

Run [Booze](https://gitlab.com/booze-tools/booze) through Heroic Games Launcher.

### How to use

Launch [Heroic Games Launcher](https://github.com/Heroic-Games-Launcher/HeroicGamesLauncher) client. Go to settings tab -> Wine -> Proton. Press green button with `+` and search for rarity binary. You're done.
